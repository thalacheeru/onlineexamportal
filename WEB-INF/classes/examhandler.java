

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.jasper.tagplugins.jstl.core.Redirect;

/**
 * Servlet implementation class examhandler
 */
@WebServlet("/examhandler")
public class examhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public examhandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();

		HttpSession stud=request.getSession(true);
				if(stud==null)
				{
					response.sendRedirect("index.html");
				}
				int count=0;
				try{
		count=Integer.parseInt(stud.getAttribute("examcount")+"");
				}
				catch(NumberFormatException e)
				{
					System.out.println(e);
					response.sendRedirect("index.html");
				}
		int score=0;
		int total=count;
		while(count!=0)
		{
			String temp=request.getParameter(count+"");
			String ans=stud.getAttribute(count+"A")+"";

			ans=ans.replace("*"," ");	
			System.out.println(temp);
			--count;
			if(temp!=null)
			{
				temp=temp.replace("*"," ");
				if(ans.equals(temp))
				{
					score++;
				}
			}
		}
		String quizname=stud.getAttribute("quizname")+"";
		String studentname=stud.getAttribute("studname")+"";
		System.out.println("score:"+score);
		
		try {
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			System.out.println("insert into scores values('"+studentname+"','"+quizname+"','"+total+"','"+score+"')");
			stmt.executeUpdate("insert into scores values('"+studentname+"','"+quizname+"','"+total+"','"+score+"')");
			stud.setAttribute("score",score);
			stud.setAttribute("total", total);
			//response.sendRedirect("examcomplete.jsp");
			out.println("<h1 align=center>Thank you for writing exam.Regular practice of writing will always help you.All the best<br>");out.println("Your test is completed"); 		out.println("<br> Your score is  "+score+"  out of "+total);
		     stud.invalidate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		

	}

}
