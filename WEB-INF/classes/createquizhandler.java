import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Servlet implementation class createquizhandler
 */
@WebServlet("/createquizhandler")
public class createquizhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public createquizhandler() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try {
			String courseid=request.getParameter("courseid");
			String qid=request.getParameter("qid");
			String qname=request.getParameter("qname");
			String noq=request.getParameter("noq");
			String st=request.getParameter("st");
			String et=request.getParameter("et");
			String duration=request.getParameter("dur");
			String date=request.getParameter("date");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			ResultSet rs=stmt.executeQuery("select quizid from quiz where quizid='"+qid+"'");
			if(rs.next())
			{
				out.println("<h1>quiz id already exists</h1>");
			}
			else
			{
				HttpSession session=request.getSession(true);
				stmt.executeUpdate("insert into quiz values('"+courseid+"','"+qid+"','"+qname+"','"+noq+"','"+st+"','"+et+"','"+duration+"','"+date+"')");
				stmt.executeUpdate("insert into quiznames values('"+qname+"')");
				stmt.executeUpdate("create table "+qname+"(questions varchar(1000))");
				session.setAttribute("quizname",qname);
				response.sendRedirect("Addquestions.jsp");
			}
				
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
