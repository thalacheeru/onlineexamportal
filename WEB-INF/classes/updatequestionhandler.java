

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class updatequestionhandler
 */
@WebServlet("/updatequestionhandler")
public class updatequestionhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updatequestionhandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		String quesid="";
		try
		{
			 HttpSession s1=request.getSession(true);
		     Enumeration<String> e =s1.getAttributeNames();
		     while(e.hasMoreElements())
			{
				 String s2=(String)e.nextElement();
				 if(s2.equals("quesids"))
				 {
					 quesid=(String)s1.getAttribute(s2);
				 }
			}
		    s1.invalidate();
			String question=request.getParameter("question");
			String questiontype=request.getParameter("questiontype");
			String options=request.getParameter("options");
			String ans=request.getParameter("ans");
			Statement stmt=con.createStatement();
				stmt.executeUpdate("use wpproject");
				stmt.executeUpdate("UPDATE question set questionname='"+question+"',questiontype='"+questiontype+"',options='"+options+"',answer='"+ans+"' where questionid='"+quesid+"';");
				response.sendRedirect("updatequestion.jsp");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
