

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class addquetoquizhandler
 */
@WebServlet("/addquetoquizhandler")
public class addquetoquizhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public addquetoquizhandler() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println(" i entered den\n\n\n");
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try {
			
			HttpSession session=request.getSession(true);
			int count=Integer.parseInt(session.getAttribute("count")+"");
			String quizname=session.getAttribute("quizname")+"";
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			for(int i=0;i<count;i++)
			{
				String temp=session.getAttribute("var"+i)+"";
				stmt.executeUpdate("insert into "+quizname+" values("+temp+")");
				System.out.println(session.getAttribute("var"+i));
			}
			response.sendRedirect("Addquestions.jsp");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
