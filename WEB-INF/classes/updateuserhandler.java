

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class updateuserhandler
 */
@WebServlet("/updateuserhandler")
public class updateuserhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public updateuserhandler() {
		super();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		String username="";
		try
		{
			 HttpSession s1=request.getSession(true);
		     Enumeration<String> e =s1.getAttributeNames();
		     while(e.hasMoreElements())
			{
				 String s2=(String)e.nextElement();
				 if(s2.equals("users"))
				 {
					 username=(String)s1.getAttribute(s2);
				 }
				 out.println(s2+"::element");
				 out.println(username+"::entered</br>");
			}
		     s1.invalidate();
//		     out.println("hello:"+request.getParameter("uname"));
			String firstname=request.getParameter("fname");
			String lastname=request.getParameter("lname");
			String pass=request.getParameter("pswd");
			String confirmpassword=request.getParameter("confmpswd");
			String email=request.getParameter("email");
			String dob=request.getParameter("DOB");
			String phonenumber=request.getParameter("number");
			String gender=request.getParameter("gender");
			String address=request.getParameter("address");
			//address=address.replaceAll("  ","");
			Statement stmt=con.createStatement();
			out.println(username+"</br>");
			if(pass.equals(confirmpassword))
			{
				stmt.executeUpdate("use wpproject");
				out.println("UPDATE user set firstname='"+firstname+"',lastname='"+lastname+"',password='"+pass+"',emailid='"+email+"',dob='"+dob+"',phonenumber='"+phonenumber+"',gender='"+gender+"',address='"+address+"'where username='"+username+"';");
				stmt.executeUpdate("UPDATE user set firstname='"+firstname+"',lastname='"+lastname+"',password='"+pass+"',emailid='"+email+"',dob='"+dob+"',phonenumber='"+phonenumber+"',gender='"+gender+"',address='"+address+"'where username='"+username+"';");
				response.sendRedirect("updateuser.jsp");
			}
			else
				out.println("<h1>please make sure to match password and confirm password</h1>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
