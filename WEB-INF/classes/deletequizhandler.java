

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class deletequizhandler
 */
@WebServlet("/deletequizhandler")
public class deletequizhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deletequizhandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try
		{
			String qname="";
			String qid=request.getParameter("qids");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			ResultSet rs=stmt.executeQuery("select quizname from quiz where quizid='"+qid+"'");
			while(rs.next())
			{
				qname=rs.getString(1);
			}
			stmt.executeUpdate("drop table "+qname+"");
			stmt.executeUpdate("delete from quiznames where quizname='"+qname+"'");
			stmt.executeUpdate("delete from quiz where quizid='"+qid+"'");
			response.sendRedirect("deletequiz.jsp");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
