

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class signuphandler
 */
@WebServlet("/signuphandler")
public class signuphandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public signuphandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try {
			String firstname=request.getParameter("fname");
			String lastname=request.getParameter("lname");
			String username=request.getParameter("uname");
			String pass=request.getParameter("pswd");
			String confirmpassword=request.getParameter("confmpswd");
			String email=request.getParameter("email");
			String dateofbirth=request.getParameter("DOB");
			String number=request.getParameter("number");
			String gender=request.getParameter("gender");
			String address=request.getParameter("address");
			Statement stmt=con.createStatement();
			if(pass.equals(confirmpassword))
			{
				stmt.executeUpdate("use wpproject");
				out.println("insert into user values('"+firstname+"','"+lastname+"','"+username+"','"+pass+"','"+email+"','"+dateofbirth+"','"+number+"','"+gender+"','"+address+"')");
				stmt.executeUpdate("insert into user values('"+firstname+"','"+lastname+"','"+username+"','"+pass+"','"+email+"','"+dateofbirth+"','"+number+"','"+gender+"','"+address+"')");
				response.sendRedirect("index.html");
			}
			else
			{
				out.println("<h1>Make sure that enter password and confirm password to be same");
			}

		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
