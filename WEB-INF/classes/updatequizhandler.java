

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class updatequizhandler
 */
@WebServlet("/updatequizhandler")
public class updatequizhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updatequizhandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		String quizid="";
		try
		{
			 HttpSession s1=request.getSession(true);
		     Enumeration<String> e =s1.getAttributeNames();
		     while(e.hasMoreElements())
			{
				 String s2=(String)e.nextElement();
				 if(s2.equals("qids"))
				 {
					 quizid=(String)s1.getAttribute(s2);
				 }
				 out.println(s2+"::element");
				 out.println(quizid+"::entered</br>");
			}
		     s1.invalidate();
			String cid=request.getParameter("courseid");
			String qid=request.getParameter("qid");
			String qname=request.getParameter("qname");
			String noq=request.getParameter("noq");
			String st=request.getParameter("st");
			String et=request.getParameter("et");
			String dur=request.getParameter("dur");
			String date=request.getParameter("date");
			
			//address=address.replaceAll("  ","");
			Statement stmt=con.createStatement();
				stmt.executeUpdate("use wpproject");
				
				System.out.println("UPDATE quiz set courseid='"+cid+"',quizname='"+qname+"',noofquestions='"+noq+"',starttime='"+st+"',endtime='"+et+"',duration='"+dur+"',date='"+date+"' where quizid='"+quizid+"'");
				stmt.executeUpdate("UPDATE quiz set courseid='"+cid+"',quizname='"+qname+"',noofquestions='"+noq+"',starttime='"+st+"',endtime='"+et+"',duration='"+dur+"',date='"+date+"' where quizid='"+quizid+"'");
				
				response.sendRedirect("updatequiz.jsp");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

}
