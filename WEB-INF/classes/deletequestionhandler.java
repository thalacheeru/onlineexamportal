

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class deletequestionhandler
 */
@WebServlet("/deletequestionhandler")
public class deletequestionhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deletequestionhandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try
		{
			String quesid=request.getParameter("quesids");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			stmt.executeUpdate("delete from question where questionid='"+quesid+"'");
			response.sendRedirect("deletequestion.jsp");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
