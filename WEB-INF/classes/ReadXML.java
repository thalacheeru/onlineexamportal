

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

/**
 * Servlet implementation class ReadXML
 */
@WebServlet("/ReadXML")
public class ReadXML extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 public boolean isTextNode(Node n){
	        return n.getNodeName().equals("#text");
	    }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadXML() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try{
            DocumentBuilderFactory docFactory =  DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse("Questions.xml");
            out.println("<table border=2><tr><th>Name</th><th>Address</th></tr>");
            Element  element = doc.getDocumentElement(); 
            NodeList personNodes = element.getChildNodes(); 

            for (int i=0; i<personNodes.getLength(); i++){

                 Node emp = personNodes.item(i);
                 if (isTextNode(emp))
                 continue;

                 NodeList NameDOBCity = emp.getChildNodes(); 
                 out.println("<tr>");

                 for (int j=0; j<NameDOBCity.getLength(); j++ ){

                     Node node = NameDOBCity.item(j);
                     if ( isTextNode(node)) 
                     continue;
                    out.println("<td>"+(node.getFirstChild().getNodeValue())+"</td>");

                 } 

                 out.println("</tr>");
             }

             out.println("</table>");

        }

        catch(Exception e){
            System.out.println(e);
        }
    }
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
