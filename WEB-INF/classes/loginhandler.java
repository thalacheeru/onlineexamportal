

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class loginhandler
 */
@WebServlet("/loginhandler")
public class loginhandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public loginhandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html"); // informing the client that which format of data/response will be send
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try
		{
			String username=request.getParameter("uname");
			String password=request.getParameter("pswd");
			Statement stmt=con.createStatement();
			stmt.executeUpdate("use wpproject");
			ResultSet rs=stmt.executeQuery("select username,password from user where username='"+username+"'and password='"+password+"'");
			if(rs.next())
			{
				if(username.equals("admin")&&password.equals("admin"))
				{
					response.sendRedirect("Home.html");
				}
				else
				{
					HttpSession stud=request.getSession(true);
					stud.setAttribute("studname",username);
					response.sendRedirect("student.jsp");
				}
			}
			else
			{
				out.println("<h1>user_name or password you entered is wrong</h1>");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
