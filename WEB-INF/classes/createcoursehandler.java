

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class createcoursehandler
 */
@WebServlet("/createcoursehandler")
public class createcoursehandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public createcoursehandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		PrintWriter out= response.getWriter(); 
		DBconnection db=new DBconnection();
		Connection con=db.getConnection();
		try {
			String courseid=request.getParameter("cid");
			String coursename=request.getParameter("cname");
			String offeredby=request.getParameter("offeredby");
			String description=request.getParameter("description");
			Statement stmt=con.createStatement();
				stmt.executeUpdate("use wpproject");
				//out.println("insert into course values('"+courseid+"','"+coursename+"','"+offeredby+"','"+description+"')");
				stmt.executeUpdate("insert into course values('"+courseid+"','"+coursename+"','"+offeredby+"','"+description+"')");
				response.sendRedirect("Home.html");
			/*else
			{
				out.println("<h1>Make sure that enter password and confirm password to be same");
			}*/

		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
