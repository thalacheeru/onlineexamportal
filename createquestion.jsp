<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>create question</title>
<style type="text/css">
    body 
	{
	background-color:#E0ECF8
	}
    ul{
        padding: 0;
        list-style: none;
    }
    ul li{
        float: left;
        width: 100px;
        text-align: center;
        line-height: 21px;
    }
    ul li a{
        display: block;
        padding: 0px 10px;
        color: black;
        background: #DEB887;
        text-decoration: none;
    }
    ul li a:hover{
        color: black;
        background: #939393;
    }
    ul li ul{
        display: none;
    }
    ul li:hover ul{
        display: block;
    }
	 #regsection
    {
	position:fixed ;
    top: 10%;
    right: 0%;
    text-align:center;
    padding:10%;
    }
  #header
{
    text-align:left;
    padding:0px;
}
table.tbl_effect
{
 text-align:left;
background-color:#C0C0C0;
padding:20px;
 border: 2px solid;
    border-radius: 10px;
}
</style>
 <script >
    function myFunction() {
    document.getElementById("myForm").reset();
    }
</script>
</head>
<body>
<div id ="header">
  <img src="qm.png" alt="Quiz image" style="width:500px;height:100px;align:center">

 <br>
 <br>
 
 </div>
    <ul>
        <li><a href="#">User &#9662;</a>
		 <ul>
               <li><a href="createuser.html">Create</a></li>
                <li><a href="updateuser.jsp">Update</a></li>
                <li><a href="deleteuser.jsp">Delete</a></li>
            </ul></li>
        <li><a href="#">Course &#9662; </a>
		  <ul>
                <li><a href="createcourse.html">Create</a></li>
                <li><a href="updatecourse.jsp">Update</a></li>
                <li><a href="deletecourse.jsp">Delete</a></li>
            </ul></li>
		<li><a href="#">Quiz &#9662;</a>
		 <ul>
                <li><a href="createquiz.jsp">Create</a></li>
                <li><a href="updatequiz.jsp">Update</a></li>
                <li><a href="deletequiz.jsp">Delete</a></li>
            </ul></li>	
		<li><a href="#">Question &#9662;</a>
		 <ul>
                <li><a href="createquestion.jsp">Create</a></li>
                <li><a href="updatequestion.jsp">Update</a></li>
                <li><a href="deletequestion.jsp">Delete</a></li>
            </ul></li>		
        <li><a href="AboutUs.html">About Us </a>
          
        </li>
        <li><a href="Help.html">Help</a></li>
    </ul>
<br>
<br>
  <div id="regsection">
  <form action="createquestionhandler" method="post" ID="myForm" name="index"   onsubmit="return validateForm()" >
  <fieldset>
    <legend><p><b><font size="3">Create Question</b></font></p></legend>
  <table class="tbl_effect" align= "center">

  <tr>
<td>
 <label >Question Id </label>
</td>
<td>
 <input type="text"  id="questionid" name="quesid" required>
</td>
</tr>
<tr>
<td>
 <label >Question</label>
</td>

<td><textarea rows="2" cols="17" name=question></textarea></td>

</tr>

<tr>
<td>
 <label >question type</label>
</td>

<td><textarea rows="1" cols="17" name=questiontype></textarea></td>

</tr>

<tr>
<td><label>Options</label></td>

<td><textarea rows="2" cols="17" name=options value="">"separate options by semicolon"</textarea></td>
</tr>

<tr>
<td> <label >Answer  </label></td>
<td> <input type="text" id="answer" name="ans" required></td>
</tr>


<tr>
<td></td>
<td><button type="Submit" >Create</button><input type="BUTTON" onclick="myFunction()" value="Clear"></td>
</tr>

<tr>
<td></td>
<td></td>
</tr>
  </table>
  </fieldset>

  </form>
  </div>
</body>
</html>