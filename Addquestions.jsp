<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
	function display() {
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else { // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("idq").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "displayavailableque.jsp", true);
		xmlhttp.send();
	}
	

	function showUser(str) {
		if (str == "") {
			document.getElementById("addque").innerHTML = "";
			return;
		}
		if (window.XMLHttpRequest) {
			// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else { // code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("addque").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "displayquestions.jsp?q=" + str, true);
		xmlhttp.send();
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Adding questions</title>
<link rel="stylesheet" href="questionsadd.css">
</head>
<body>
	<div class="questiontype">
		<table align="center">
			<%
				String fi;
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306", "root", "root");
				try {
					Statement stmt = con.createStatement();
					stmt.executeUpdate("use wpproject");
					ResultSet rs = stmt
							.executeQuery("select distinct questiontype from question");
			%>

			<tr>
				<td><label>Question types </label></td>
				<td><select name="Questiontypes" onclick="showUser(this.value)">
						<%
							while (rs.next()) {
						%>
						<option><%=rs.getString(1)%></option>
						<%
							}
						%>

				</select></td>
			</tr>

			<%
				//**Should I input the codes here?**
				} catch (Exception e) {
					out.println("wrong entry" + e);
				}
			%>

			</form>
		
		</table>
	</div>
	<div class="displayque" id="addque">
		<b>Questions will be listed here.</b>
	
	</div>
	<div class="s" id="su">
	<form action="addquetoquizhandler" method="post" ID="myForm"
				name="dq">
				<input type="submit" value="submit" />
			</form>
	</div>
	<div class="dq" id="idq">
		<button type="button" onclick="display()">display available
			questions</button>
			
	</div>
	<div class="bottom" id="bot">
	<a href="Home.html">exit from page </a>
	</div>
	<div class="preview" id="pw">
	<form action="preview.jsp" method="post" ID="myForm"
				name="pre">
				<input type="submit" value="preview" />
			</form>
</body>
</html>