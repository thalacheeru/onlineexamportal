<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Update Question</title>
<style type="text/css">
body {
	background-color: #E0ECF8
}

ul {
	padding: 0;
	list-style: none;
}

ul li {
	float: left;
	width: 100px;
	text-align: center;
	line-height: 21px;
}

ul li a {
	display: block;
	padding: 0px 10px;
	color: black;
	background: #DEB887;
	text-decoration: none;
}

ul li a:hover {
	color: black;
	background: #939393;
}

ul li ul {
	display: none;
}

ul li:hover ul {
	display: block;
}

#regsection {
	position: fixed;
	top: 10%;
	right: 0%;
	text-align: center;
	padding: 10%;
}

#header {
	text-align: left;
	padding: 0px;
}

table.tbl_effect {
	text-align: left;
	background-color: #C0C0C0;
	padding: 20px;
	border: 2px solid;
	border-radius: 10px;
}
</style>
<script>
	function myFunction() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[6].setAttribute("type", "date");
	}
	function myFunction1() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[4].setAttribute("type", "time");
	}
	function myFunction2() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[5].setAttribute("type", "time");
	}
</script>
</head>
<body>
	<div id="header">
		<img src="qm.png" alt="Quiz image"
			style="width: 500px; height: 100px; align: center"> <br> <br>

	</div>
	<ul>
		<li><a href="#">User &#9662;</a>
			<ul>
				<li><a href="createuser.html">Create User</a></li>
				<li><a href="updateuser.jsp">Update User</a></li>
				<li><a href="deleteuser.jsp">Delete User</a></li>
			</ul></li>
		<li><a href="#">Course </a>
			<ul>
				<li><a href="createcourse.html">Create Course</a></li>
				<li><a href="updatecourse.jsp">Update Course</a></li>
				<li><a href="deletecourse.jsp">Delete Course</a></li>
			</ul></li>
		<li><a href="#">Quiz </a>
			<ul>
				<li><a href="createquiz.jsp">Create Quiz</a></li>
				<li><a href="updatequiz.jsp">Update Quiz</a></li>
				<li><a href="deletequiz.jsp">Delete Quiz</a></li>
			</ul></li>
		<li><a href="#">Question </a>
			<ul>
				<li><a href="createquestion.jsp">Create Question</a></li>
				<li><a href="updatequestion.jsp">Update Question</a></li>
				<li><a href="deletequestion.jsp">Delete Question</a></li>
			</ul></li>
		<li><a href="AboutUs.html">About Us </a></li>
		<li><a href="Help.html">Help</a></li>
	</ul>
	<br>
	<br>
	<div id="regsection">
		<form action="updatequestionhandler" method="post" ID="myForm"
			name="index" onsubmit="return validateForm()">
			<fieldset>
				<legend>
					<p>
						<b><font size="3">Update Question</b></font>
					</p>
				</legend>
				<table class="tbl_effect" align="center">
					<%
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection(
								"jdbc:mysql://localhost:3306", "root", "root");
						response.setContentType("text/html"); // informing the client that which format of data/response will be send
						String s;
						try {
							Statement stmt = con.createStatement();
							stmt.executeUpdate("use wpproject");
							String questionid = request.getParameter("quesids");
							HttpSession s1 = request.getSession(true);
							s1.setAttribute("quesids", new String(questionid));
							ResultSet rs = stmt
									.executeQuery("select * from question where questionid='"
											+ questionid + "'");
							while (rs.next()) {
								//s=rs.getString(5);
								//out.println(s+"hello23");
								//request.setAttribute("fname","rs.getString(1)");
								session.setAttribute("quesid", rs.getString(1));
								session.setAttribute("question", rs.getString(2));
								session.setAttribute("questiontype", rs.getString(3));
								session.setAttribute("options", rs.getString(4));
								session.setAttribute("ans", rs.getString(5));
							}
						} catch (Exception e) {
							out.println("wrong entry" + e);
						}
					%>
					<tr>
						<td><label>Question Id </label></td>
						<td><input type="text" id="questionid" name="quesid" value="<%out.println(session.getAttribute("quesid"));%>" disabled required>
						</td>
					</tr>
					<tr>
						<td><label>Question</label></td>

						<td><textarea rows="2" cols="16" name="question" value="" required><%out.println(session.getAttribute("question"));%></textarea></td>

					</tr>
					<tr>
						<td><label>Questiontype</label></td>

						<td><textarea rows="2" cols="16" name="questiontype" value="" required><%out.println(session.getAttribute("questiontype"));%></textarea></td>
					</tr>
					<tr>
						<td><label>Options</label></td>

						<td><textarea rows="2" cols="16" name="options" value="" required><%out.println(session.getAttribute("options"));%></textarea></td>
					</tr>
					<tr>
						<td><label>Answer </label></td>
						<td><input type="text" id="answer" name="ans" value="<%out.println(session.getAttribute("ans"));%>" required></td>
					</tr>


					<tr>
						<td></td>
						<td><button type="Submit">Update</button> </td>
					</tr>

					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</fieldset>

		</form>
	</div>
</body>
</html>