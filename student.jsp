<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
HttpSession stud=request.getSession(true);
if(stud.getAttribute("studname")==null)
{
	System.out.println("bye");
	response.sendRedirect("index.html");
}
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Online Quiz</title>
<style>
  body{background-color:#E0ECF8}
 #header
{
    text-align:left;
    padding:0px;
}
</style>
<script>
function test()
{
	window.location="test.jsp";
}
function scores()
{
	window.location="viewscores.jsp";
}
</script>
</head>
<body>
<div id ="header">
  <img src="qm.png" alt="Quiz image" style="width:500px;height:100px;align:center">

 <br>
 <br>
 
 </div>
	<h1 align="center">Welcome to online quiz portal</h1>
	<table align="center">
		<tr>
			<td><input type="button" name="test" value="Take test"
				onclick="test()"></td>
			<td><input type="button" name="scores" value="View scores"
				onclick="scores()"></td>
		</tr>
	</table>
</body>
</html>