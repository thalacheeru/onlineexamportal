<%@page import="java.util.StringTokenizer"%>
<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
HttpSession stud=request.getSession(true);

if(stud.getAttribute("studname")==null)
{
	System.out.println("bye");
	response.sendRedirect("index.html");
}

Class.forName("com.mysql.jdbc.Driver");
Connection con = DriverManager.getConnection(
		"jdbc:mysql://localhost:3306","root", "root");
response.setContentType("text/html"); 
Statement stmt=con.createStatement();
stmt.executeUpdate("use wpproject");
String quizname=request.getParameter("quiz");

ResultSet rs=stmt.executeQuery("select duration from quiz where quizname='"+quizname+"'");
String t="";
while(rs.next())
{
	t=rs.getString(1);
}
String mins = request.getParameter( "mins" );
if( mins == null ) mins = t;
  
String secs = request.getParameter( "secs" );
if( secs == null ) secs = "0";
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body{background-color:#E0ECF8}
 #header
{
    text-align:left;
    padding:0px;
}
</style>
<script>
var mins = <%=mins%>; // write mins to javascript
var secs = <%=secs%>
	; // write secs to javascript
	function timer() {
		
		
		// tic tac
		if (--secs == -1) {
			secs = 59;
			--mins;
		}

		// leading zero? formatting
		if (secs < 10)
			secs = "0" + secs;
		if (mins < 10)
			mins = "0" + parseInt(mins, 10);

		// display
		document.forma.mins.value = mins;
		document.forma.secs.value = secs;

		// continue?
		if (secs == 0 && mins == 0) // time over
		{
			alert("Time up.Your answers will be auto submitted")
			document.exam.submit();
		//	window.location="examhandler";
		} else // call timer() recursively every 1000 ms == 1 sec
		{
			
			window.setTimeout("timer()", 1000);
		}
	}
</script>

<title>ONLINE QUIZ:MSIT</title>
</head>
<body>
<form action="" name="forma">
		Time remaining: <input type="text" name="mins" size="1"
			style="border: 0px solid black; text-align: right" disabled>:<input
			type="text" name="secs" size="1" style="border: 0px solid black" disabled>
		<hr>
	</form>
	
	<script>
	<!--
		timer(); // call timer() after page is loaded
	//-->
	</script>
<form action="examhandler" name="exam" method="get">
<table class="tbl_effect" align="center">
					<%
						// informing the client that which format of data/response will be send
						int count=0;
						try {
							
						    stud.setAttribute("quizname",quizname);
						    System.out.println("quizname::"+quizname);
						 rs=stmt.executeQuery("select * from question where questionid in(select questions from "+quizname+")");
						 while(rs.next())
						 {
							 out.println(++count+":");
							 out.println("Question:"+rs.getString(2));
							 out.println("<br>");
							 StringTokenizer st=new StringTokenizer(rs.getString(4),";");
							 while(st.hasMoreTokens())
							 {
								 String val=st.nextToken();
								 String temp=val.replace(" ","*");
								// out.println(st.nextToken());
								 out.println("<input type=\"radio\" name="+count+" value="+temp+">"+val+"<br>");
							 }
							 String ans=rs.getString(5);
							 ans=ans.replace(" ","*");
							 stud.setAttribute(count+"A",ans);
							 out.println("<br><br>");
						 }
						 session.setAttribute("examcount",count);
						} 
						catch (Exception e) {
							out.println("wrong entry" + e);
						}
					%>
					<tr>
						<td><button type="Submit">submit</button> </td>
					</tr>
				
				</table>
				</form>
</body>
</html>