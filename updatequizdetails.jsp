<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Update Quiz</title>
<style type="text/css">
body {
	background-color: #E0ECF8
}

ul {
	padding: 0;
	list-style: none;
}

ul li {
	float: left;
	width: 100px;
	text-align: center;
	line-height: 21px;
}

ul li a {
	display: block;
	padding: 0px 10px;
	color: black;
	background: #DEB887;
	text-decoration: none;
}

ul li a:hover {
	color: black;
	background: #939393;
}

ul li ul {
	display: none;
}

ul li:hover ul {
	display: block;
}

#regsection {
	position: fixed;
	top: 10%;
	right: 0%;
	text-align: center;
	padding: 10%;
}

#header {
	text-align: left;
	padding: 0px;
}

table.tbl_effect {
	text-align: left;
	background-color: #C0C0C0;
	padding: 20px;
	border: 2px solid;
	border-radius: 10px;
}
</style>
<script>
	function myFunction() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[6].setAttribute("type", "date");
	}
	function myFunction1() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[4].setAttribute("type", "time");
	}
	function myFunction2() {
		/* document.getElementById("myForm").reset(); */
		document.getElementsByTagName("INPUT")[5].setAttribute("type", "time");
	}
</script>
</head>
<body>
	<div id="header">
		<img src="qm.png" alt="Quiz image"
			style="width: 500px; height: 100px; align: center"> <br> <br>

	</div>
	<ul>
		<li><a href="#">User &#9662;</a>
			<ul>
				<li><a href="createuser.html">Create User</a></li>
				<li><a href="updateuser.jsp">Update User</a></li>
				<li><a href="deleteuser.jsp">Delete User</a></li>
			</ul></li>
		<li><a href="#">Course </a>
			<ul>
				<li><a href="createcourse.html">Create Course</a></li>
				<li><a href="updatecourse.jsp">Update Course</a></li>
				<li><a href="deletecourse.jsp">Delete Course</a></li>
			</ul></li>
		<li><a href="#">Quiz </a>
			<ul>
				<li><a href="createquiz.jsp">Create Quiz</a></li>
				<li><a href="updatequiz.jsp">Update Quiz</a></li>
				<li><a href="deletequiz.jsp">Delete Quiz</a></li>
			</ul></li>
		<li><a href="#">Question </a>
			<ul>
				<li><a href="createquestion.jsp">Create Question</a></li>
				<li><a href="updatequestion.jsp">Update Question</a></li>
				<li><a href="deletequestion.jsp">Delete Question</a></li>
			</ul></li>
		<li><a href="AboutUs.html">About Us </a></li>
		<li><a href="Help.html">Help</a></li>
	</ul>
	<br>
	<br>
	<div id="regsection">
		<form action="updatequizhandler" method="post" ID="myForm" name="index" 
			onsubmit="return validateForm()" >
			<fieldset>
				<legend>
					<p>
						<b><font size="3">Update Quiz</b></font>
					</p>
				</legend>
				<table class="tbl_effect" align="center">
					<%
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection(
								"jdbc:mysql://localhost:3306", "root", "root");
						response.setContentType("text/html"); // informing the client that which format of data/response will be send
						String s;
						try {
							Statement stmt = con.createStatement();
							stmt.executeUpdate("use wpproject");
							String quizid = request.getParameter("qids");
							HttpSession s1 = request.getSession(true);
							s1.setAttribute("qids", new String(quizid));
							ResultSet rs = stmt
									.executeQuery("select * from quiz where quizid='"
											+ quizid + "'");
							while (rs.next()) {
								//s=rs.getString(5);
								//out.println(s+"hello23");
								//request.setAttribute("fname","rs.getString(1)");
								session.setAttribute("cid", rs.getString(1));
								session.setAttribute("qid", rs.getString(2));
								session.setAttribute("qname", rs.getString(3));
								session.setAttribute("quizname",rs.getString(3));
								session.setAttribute("noq", rs.getString(4));
								session.setAttribute("st", rs.getString(5));
								session.setAttribute("et", rs.getString(6));
								session.setAttribute("duration",rs.getString(7));
								session.setAttribute("date", rs.getString(8));
							}
							 rs = stmt.executeQuery("select courseid from course");
							 %>
							 <tr><td><label>Course Id </label>
							 <td><select name="courseid" value="<%out.println(session.getAttribute("cid"));%>" >
								<%
									while (rs.next()) {
								%>
								<%if(rs.getString(1).equals(session.getAttribute("cid"))){%>
								<option selected><%=rs.getString(1)%></option>
								<%} else{%>
								<option><%=rs.getString(1)%></option>
								<%
								}}
								%>
						</select></td>


						<%
							} catch (Exception e) {
								out.println("wrong entry" + e);
							}
						%>
						</tr>
					
					<tr>
						<td><label>Quiz Id </label></td>
						<td><input type="text" id="quizid" name="qid"
							value="<%out.println(session.getAttribute("qid"));%>" disabled required>
						</td>
					</tr>
					<tr>
						<td><label>Quiz Name </label></td>
						<td><input type="text" id="quizname" name="qname"
							value="<%out.println(session.getAttribute("qname"));%>" required>
						</td>
					</tr>

					<tr>
						<td><label>No of Questions</label></td>
						<td><input type="text" id="noq" name="noq"
							value="<%out.println(session.getAttribute("noq"));%>" required></td>
					</tr>
					<tr>
						<td><label>Start Time</label></td>
						<td><input type="text" id="time" name="st"
							value="<%out.println(session.getAttribute("st"));%>" required></td>
						<td>
							<button onclick="myFunction1()">change</button>
						</td>
					</tr>
					<tr>
						<td><label>End Time</label></td>
						<td><input type="text" id="time" name="et"
							value="<%out.println(session.getAttribute("et"));%>" required></td>
						<td>
							<button onclick="myFunction2()">change</button>
						</td>
					</tr>
					
					<tr>
						<td><label>Duration</label></td>
						<td><input type="text" id="time" name="dur"
							value="<%out.println(session.getAttribute("duration"));%>" required></td>
						
					</tr>
					
					
					

					<tr>
						<td><label>Date </label></td>
						<td><input type="text" id="date" name="date"
							value="<%out.println(session.getAttribute("date"));%>" required>
						</td>
						<td>
							<button onclick="myFunction()">change</button>
						</td>
					</tr>

					<tr>
						<td><button type="Submit">Update</button></td>
						<td><a href="Addquestions.jsp">To add new questions</a>
					</tr>

					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</fieldset>

		</form>
	</div>
</body>
</html>