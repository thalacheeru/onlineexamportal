function validate()
{ 
	if( document.Registration.fname.value == "" )
	{
		alert( "Please provide your first name!" );
		document.Registration.fname.focus() ;
		return false;
	}
	if( document.Registration.lname.value == "" )
	{
		alert( "Please provide your last name!" );
		document.Registration.lname.focus() ;
		return false;
	}

	if( document.Registration.uname.value == "" )
	{
		alert( "Please provide your user name!" );
		document.Registration.uname.focus() ;
		return false;
	}
	var email = document.Registration.email.value;
	atpos = email.indexOf("@");
	dotpos = email.lastIndexOf(".");
	if (email == "" || atpos < 1 || ( dotpos - atpos < 2 )) 
	{
		alert("Please enter correct email ID")
		document.Registration.email.focus() ;
		return false;
	}
	if( document.Registration.DOB.value == "" )
	{
		alert( "Please provide your DOB!" );
		document.Registration.DOB.focus() ;
		return false;
	}
	if( document.Registration.number.value == "" ||
			isNaN( document.Registration.number.value) ||
			document.Registration.number.value.length != 10 )
	{
		alert( "Please provide a Mobile No in the format 123." );
		document.Registration.number.focus() ;
		return false;
	}
	if ( ( Registration.gender[0].checked == false ) && ( Registration.gender[1].checked == false ) )
	{
		alert ( "Please choose your Gender: Male or Female" );
		return false;
	}  
	if( document.Registration.address.value == "" )
	{
		alert( "Please provide your address!" );
		document.Registration.address.focus() ;
		return false;
	}
	return( true );
}