<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>delete user</title>
<style type="text/css">
body {
	background-color: #E0ECF8
}

ul {
	padding: 0;
	list-style: none;
}

ul li {
	float: left;
	width: 100px;
	text-align: center;
	line-height: 21px;
}

ul li a {
	display: block;
	padding: 0px 10px;
	color: black;
	background: #DEB887;
	text-decoration: none;
}

ul li a:hover {
	color: black;
	background: #939393;
}

ul li ul {
	display: none;
}

ul li:hover ul {
	display: block;
}

#regsection {
	position: fixed;
	top: 10%;
	right: 0%;
	text-align: center;
	padding: 10%;
}

#header {
	text-align: left;
	padding: 0px;
}

table.tbl_effect {
	text-align: left;
	background-color: #C0C0C0;
	padding: 20px;
	border: 2px solid;
	border-radius: 10px;
}
</style>
<script>
	function myFunction() {
		document.getElementById("myForm").reset();
	}
</script>
</head>
<body>
	<div id="header">
		<img src="qm.png" alt="Quiz image"
			style="width: 500px; height: 100px; align: center"> <br> <br>

	</div>
	<ul>
		<li><a href="#">User &#9662;</a>
			<ul>
				<li><a href="createuser.html">Create User</a></li>
				<li><a href="updateuser.jsp">Update User</a></li>
				<li><a href="deleteuser.jsp">Delete User</a></li>
			</ul></li>
		<li><a href="#">Course </a>
			<ul>
				<li><a href="createcourse.html">Create Course</a></li>
				<li><a href="updatecourse.jsp">Update Course</a></li>
				<li><a href="deletecourse.jsp">Delete Course</a></li>
			</ul></li>
		<li><a href="#">Quiz </a>
			<ul>
				<li><a href="createquiz.jsp">Create Quiz</a></li>
				<li><a href="updatequiz.jsp">Update Quiz</a></li>
				<li><a href="deletequiz.jsp">Delete Quiz</a></li>
			</ul></li>
		<li><a href="#">Question </a>
			<ul>
				<li><a href="createquestion.jsp">Create Question</a></li>
				<li><a href="updatequestion.jsp">Update Question</a></li>
				<li><a href="deletequestion.jsp">Delete Question</a></li>
			</ul></li>
		<li><a href="AboutUs.html">About Us </a></li>
		<li><a href="Help.html">Help</a></li>
	</ul>
	<br>
	<br>
	<div id="regsection">
		<form action="deleteuserhandler" method="post" ID="myForm" name="index" 
			onsubmit="return validateForm()" >
			<fieldset>
				<legend>
					<p>
						<b><font size="3">Delete User</b></font>
					</p>
				</legend>
				<table class="tbl_effect" align="center">
					<%
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "root");
						try {
							Statement stmt = con.createStatement();
							stmt.executeUpdate("use wpproject");
							ResultSet rs = stmt.executeQuery("select username from user");
					%>

					<tr>
					<td><label>user id </label></td>
						<td><select name="users">
							<%
								while (rs.next()) {
							%>
							<option><%=rs.getString(1)%></option>
							<%
								}
							%>
						</select>
					</tr>

					<%
						//**Should I input the codes here?**
						} catch (Exception e) {
							out.println("wrong entry" + e);
						}
					%>

				




					<tr>
						<td></td>
						<td> <button type="Submit">Delete</button> </td>
					</tr>

					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</fieldset>

		</form>
	</div>
</body>
</html>