function validate()
{ 
	if( document.crcourse.cid.value == "" )
	{
		alert( "Please provide course id!" );
		document.crcourse.cid.focus() ;
		return false;
	}
	if( document.crcourse.cname.value == "" )
	{
		alert( "Please provide course name!" );
		document.crcourse.cname.focus() ;
		return false;
	}

	if( document.crcourse.offeredby.value == "" )
	{
		alert( "Please provide offered by name!" );
		document.crcourse.offeredby.focus() ;
		return false;
	}

	if( document.crcourse.description.value == "" )
	{
		alert( "Please provide description to course!" );
		document.crcourse.description.focus() ;
		return false;
	}

	return( true );
}