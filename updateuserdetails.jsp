<%@ page import="java.sql.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<title>Update User</title>
<style type="text/css">
body {
	background-color: #E0ECF8
}

ul {
	padding: 0;
	list-style: none;
}

ul li {
	float: left;
	width: 100px;
	text-align: center;
	line-height: 21px;
}

ul li a {
	display: block;
	padding: 0px 10px;
	color: black;
	background: #DEB887;
	text-decoration: none;
}

ul li a:hover {
	color: black;
	background: #939393;
}

ul li ul {
	display: none;
}

ul li:hover ul {
	display: block;
}

#regsection {
	position: fixed;
	top: 10%;
	right: 0%;
	text-align: center;
	padding: 10%;
}

#header {
	text-align: left;
	padding: 0px;
}

table.tbl_effect {
	text-align: left;
	background-color: #C0C0C0;
	padding: 20px;
	border: 2px solid;
	border-radius: 10px;
}
</style>
<script>
function myFunction() {
    document.getElementsByTagName("INPUT")[6].setAttribute("type", "date"); 
}
</script>
</head>
<body>
	<div id="header">
		<img src="qm.png" alt="Quiz image"
			style="width: 500px; height: 100px; align: center"> <br> <br>

	</div>
	<ul>
		<li><a href="#">User &#9662;</a>
			<ul>
				<li><a href="createuser.html">Create User</a></li>
				<li><a href="updateuser.jsp">Update User</a></li>
				<li><a href="deleteuser.jsp">Delete User</a></li>
			</ul></li>
		<li><a href="#">Course </a>
			<ul>
				<li><a href="createcourse.html">Create Course</a></li>
				<li><a href="updatecourse.jsp">Update Course</a></li>
				<li><a href="deletecourse.jsp">Delete Course</a></li>
			</ul></li>
		<li><a href="#">Quiz </a>
			<ul>
				<li><a href="createquiz.jsp">Create Quiz</a></li>
				<li><a href="updatequiz.jsp">Update Quiz</a></li>
				<li><a href="deletequiz.jsp">Delete Quiz</a></li>
			</ul></li>
		<li><a href="#">Question </a>
			<ul>
				<li><a href="createquestion.jsp">Create Question</a></li>
				<li><a href="updatequestion.jsp">Update Question</a></li>
				<li><a href="deletequestion.jsp">Delete Question</a></li>
			</ul></li>
		<li><a href="AboutUs.html">About Us </a></li>
		<li><a href="Help.html">Help</a></li>
	</ul>
	<br>
	<br>
	<div id="regsection">
		<form action="updateuserhandler" method="post" ID="myForm"
			name="index" onsubmit="return validateForm()">
			<fieldset>
				<legend>
					<p>
						<b><font size="3">Update User</b></font>
					</p>
				</legend>
				<table class="tbl_effect" align="center">
					<%
						Class.forName("com.mysql.jdbc.Driver");
						Connection con = DriverManager.getConnection(
								"jdbc:mysql://localhost:3306", "root", "root");
						response.setContentType("text/html"); // informing the client that which format of data/response will be send
						String s;
						try {
							Statement stmt = con.createStatement();
							stmt.executeUpdate("use wpproject");
							String username = request.getParameter("users");
							 HttpSession s1=request.getSession(true);
							 s1.setAttribute("users",new String(username));
							ResultSet rs = stmt
									.executeQuery("select * from user where username='"
											+ username + "'");
							while (rs.next()) {
								//s=rs.getString(5);
								//out.println(s+"hello23");
								//request.setAttribute("fname","rs.getString(1)");
								session.setAttribute("fname", rs.getString(1));
								session.setAttribute("lname", rs.getString(2));
								session.setAttribute("uname", rs.getString(3));
								session.setAttribute("pswd", rs.getString(4));
								session.setAttribute("email", rs.getString(5));
								session.setAttribute("DOB", rs.getString(6));
								session.setAttribute("number", rs.getString(7));
								session.setAttribute("gender", rs.getString(8));
								session.setAttribute("address", rs.getString(9));
							}

						} catch (Exception e) {
							out.println("wrong entry" + e);
						}
					%>
					<tr>
						<td><label>FirstName</label></td>
						<td><input type="text" id="firstname" name="fname"
							value="<%out.println(session.getAttribute("fname"));%>" required>
						</td>
					</tr>
					<tr>
						<td><label>LastName</label></td>
						<td><input type="text" id="lastname" name="lname"
							value="<%out.println(session.getAttribute("lname"));%>" required>
						</td>
					</tr>
					<tr>
						<td><label for="Username">Username</label></td>
						<td><input type="text" id="username" name="uname"
							value="<%out.println(session.getAttribute("uname"));%>" disabled required>
						</td>
					</tr>

					<tr>
						<td><label for="Password">Password</label></td>
						<td><input type="text" id="password" name="pswd"
							value="<%out.println(session.getAttribute("pswd"));%>" required><br></td>
					</tr>

					<tr>
						<td><label for="Password">Confirm Password</label></td>
						<td><input type="password" id="confmpassword"
							name="confmpswd" required><br></td>
					</tr>

					<tr>
						<td><label>Email Id </label></td>
						<td><input type="email" id="email" name="email"
							value="<%out.println(session.getAttribute("email"));%>" required>
						</td>
					</tr>

					<tr>
						<td><label>DOB</label></td>
						<td><input type="TEXT" id="DOB" name="DOB"
							value="<%out.println(session.getAttribute("DOB"));%>" required></td>
								<td>
								<button onclick="myFunction()">change</button>
								</td>
					</tr>
					<tr>
						<td><label>Phone number</label></td>
						<td><input type="TEXT" id="number" name="number"
							value="<%out.println(session.getAttribute("number"));%>" required></td>
					</tr>
					<tr>
						<td><label>Gender</label></td>
						<td><input type="text" name="gender"
							value="<%out.println(session.getAttribute("gender"));%>">
						</td>
					</tr>
					<tr>
						<td><label>Address</label></td>
						<td><textarea rows="4" STYLE="background-color: #E0ECF8;"
								name="address" cols="20" value="address">
						<%
							out.println(session.getAttribute("address"));
						%>
</textarea></td>
					</tr>
					<tr>
						<td></td>
						<td><button type="Submit">Update</button> </td>
					</tr>

					<tr>
						<td></td>
						<td></td>
					</tr>
				</table>
			</fieldset>

		</form>
	</div>
</body>
</html>